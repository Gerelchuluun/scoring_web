

export const getStatusTxt = (status) => {
    let statusTxt = '';
    switch (status){
        case 'QUESTIONABLE' :
            statusTxt = 'ЭРГЭЛЗЭЭТЭЙ'
            break;
        case 'POSSIBLE' :
            statusTxt = 'БОЛОМЖТОЙ'
            break;
        default :
            statusTxt = 'БОЛОМЖГҮЙ'
            break;     
    }
    return statusTxt;
}